package com.sample.agrofoods.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.demono.AutoScrollViewPager;
import com.sample.agrofoods.Activities.CartActivity;
import com.sample.agrofoods.Activities.CategoriesActivity;
import com.sample.agrofoods.Adapters.AutoViewPager;
import com.sample.agrofoods.Adapters.CategoriesRecyclerAdapter;
import com.sample.agrofoods.Adapters.ProductsHorizontalRecyclerAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.CartCountResponse;
import com.sample.agrofoods.Models.HomeDataResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindFoodFragment extends Fragment {
    RecyclerView recycler_categories,recycler_products;
    CategoriesRecyclerAdapter recyclerAdapter;
    SliderView sliderView;
    RelativeLayout relativecategories;
    TextView txt_viewall;
    @BindView(R.id.viewPager)
    AutoScrollViewPager viewPager;

    @BindView(R.id.img_cart)
    ImageView img_cart;
    @BindView(R.id.relative_count)
    RelativeLayout relative_count;

    @BindView(R.id.text_count)
    TextView text_count;
    AppController appController;
    PrefManager prefManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.activity_home, container, false);
        ButterKnife.bind(this,v);
        recycler_categories=v.findViewById(R.id.recycler_categories);
        recycler_products=v.findViewById(R.id.recycler_products);

        relativecategories = v.findViewById(R.id.relativecategories);
        txt_viewall = v.findViewById(R.id.txt_viewall);

        prefManager = new PrefManager(getContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        String token = profile.get("Token");

        appController = (AppController) getActivity().getApplication();
        boolean isConnected = appController.isConnection();
        if (isConnected) {

            getData();
            cartCount(token);

        }
        else {
            Toast.makeText(getActivity(), "No Internet Connection!...", Toast.LENGTH_SHORT).show();

        }

        txt_viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), CategoriesActivity.class));
            }
        });

        img_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CartActivity.class));
            }
        });

        return v;

    }

    private void getData() {

        ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<HomeDataResponse> call= RetrofitClient.getInstance().getApi().HomeData();
        call.enqueue(new Callback<HomeDataResponse>() {
            @Override
            public void onResponse(Call<HomeDataResponse> call, Response<HomeDataResponse> response) {
                if (response.isSuccessful());
                HomeDataResponse homeDataResponse=response.body();
                if (homeDataResponse.getCode() == 200){
                    progressDialog.dismiss();
                    HomeDataResponse.DataBean dataBean=homeDataResponse.getData();
                    List<HomeDataResponse.DataBean.BannersBean> bannersBeanList=dataBean.getBanners();
                    List<HomeDataResponse.DataBean.CategoriesBean> categoriesBeanList=dataBean.getCategories();
                    List<HomeDataResponse.DataBean.ProductsBean> productsBeanList=dataBean.getProducts();

                    Banners(bannersBeanList);
                    Categories(categoriesBeanList);
                    Products(productsBeanList);
                }
                else if (homeDataResponse.getCode() == 204){
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), homeDataResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<HomeDataResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void cartCount(final String userId) {

        Call<CartCountResponse> call= RetrofitClient.getInstance().getApi().CartCount(userId);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful());
                CartCountResponse cartCountResponse=response.body();

                if (cartCountResponse.getCode() == 200){
                    CartCountResponse.DataBean dataBean=cartCountResponse.getData();
                    int cartindex =dataBean.getCount();

                    if (String.valueOf(cartindex).equals("0")){
                        relative_count.setVisibility(View.GONE);
                    }
                    else {
                        relative_count.setVisibility(View.VISIBLE);
                    }

                    text_count.setText(String.valueOf(cartindex));

                }

                else if (cartCountResponse.getCode() == 401){
                    Toast.makeText(getContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(getContext(), "Server Error...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Banners(List<HomeDataResponse.DataBean.BannersBean> bannersBeanList) {
        AutoViewPager mAdapter = new AutoViewPager(bannersBeanList, getActivity());
        viewPager.setAdapter(mAdapter);
        // optional start auto scroll
        viewPager.startAutoScroll();
    }

    private void Categories(List<HomeDataResponse.DataBean.CategoriesBean> categoriesBeanList) {
        recyclerAdapter = new CategoriesRecyclerAdapter(getContext(),categoriesBeanList);
        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext());
        layoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        recycler_categories.setLayoutManager(layoutManager1);
        recycler_categories.setNestedScrollingEnabled(false);
        recycler_categories.setAdapter(recyclerAdapter);
    }

    private void Products(List<HomeDataResponse.DataBean.ProductsBean> productsBeanList) {
        ProductsHorizontalRecyclerAdapter productsHorizontalRecyclerAdapter = new ProductsHorizontalRecyclerAdapter(getContext(),productsBeanList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        recycler_products.setLayoutManager(mLayoutManager);
     //   recycler_products.addItemDecoration(new FindFoodFragment.GridSpacingItemDecoration(3, dpToPx(2), true));
        recycler_products.setItemAnimator(new DefaultItemAnimator());
        recycler_products.setAdapter(productsHorizontalRecyclerAdapter);

    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }



}
