package com.sample.agrofoods.Apis;
import com.sample.agrofoods.Models.AddAddressResponse;
import com.sample.agrofoods.Models.AddCartResponse;
import com.sample.agrofoods.Models.AddressListResponse;
import com.sample.agrofoods.Models.AllCategoriesResponse;
import com.sample.agrofoods.Models.AllProductsResponse;
import com.sample.agrofoods.Models.CartCountResponse;
import com.sample.agrofoods.Models.CartDeleteResponse;
import com.sample.agrofoods.Models.CartListingResponse;
import com.sample.agrofoods.Models.CheckoutResponse;
import com.sample.agrofoods.Models.DeleteAddressResponse;
import com.sample.agrofoods.Models.HomeDataResponse;
import com.sample.agrofoods.Models.LoginResponse;
import com.sample.agrofoods.Models.MyOrdersResponse;
import com.sample.agrofoods.Models.NotificationsResponse;
import com.sample.agrofoods.Models.OrderDetailsResponse;
import com.sample.agrofoods.Models.OrderPlaceResponse;
import com.sample.agrofoods.Models.ProductDetailsResponse;
import com.sample.agrofoods.Models.ProductSearchResponse;
import com.sample.agrofoods.Models.ProductVariationResponse;
import com.sample.agrofoods.Models.ProfileResponse;
import com.sample.agrofoods.Models.RegistrationResponse;
import com.sample.agrofoods.Models.ResendOtpREsponse;
import com.sample.agrofoods.Models.SubCategoriesResponse;
import com.sample.agrofoods.Models.UpdateAddressResponse;
import com.sample.agrofoods.Models.UpdateCartResponse;
import com.sample.agrofoods.Models.UpdateProfileResponse;
import com.sample.agrofoods.Models.VerifyOtpResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Api {

    @GET("home")
    Call<HomeDataResponse> HomeData();

    @GET("all-categories")
    Call<AllCategoriesResponse> AllCategories();

    @FormUrlEncoded
    @POST("sub-categories")
    Call<SubCategoriesResponse> Subcategory(@Field("category_id") String category_id);

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> Login(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("signup")
    Call<RegistrationResponse> Registration(@Field("phone") String phone, @Field("name") String name, @Field("email") String email);

    @FormUrlEncoded
    @POST("verify-opt")
    Call<VerifyOtpResponse> VerifyOtp(@Field("phone") String phone, @Field("otp") String otp);

    @FormUrlEncoded
    @POST("resend-opt")
    Call<ResendOtpREsponse> ResendOtp(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("my-profile")
    Call<ProfileResponse> Profile(@Field("token") String token);

    @FormUrlEncoded
    @POST("category-products")
    Call<AllProductsResponse> ProductListing(@Field("category_id") String category_id);

    @FormUrlEncoded
    @POST("product-details")
    Call<ProductDetailsResponse> ProductDetails(@Field("product_id") String product_id, @Field("token") String token);

    @FormUrlEncoded
    @POST("product-veristions")
    Call<ProductVariationResponse> ProductVariations(@Field("product_id") String product_id);

    @FormUrlEncoded
    @POST("view-cart")
    Call<CartListingResponse> CartListing(@Field("token") String token);

    @FormUrlEncoded
    @POST("add-cart")
    Call<AddCartResponse> AddCart(@Field("product_id") String product_id, @Field("quantity") String quantity,
                                  @Field("token") String token, @Field("veriation_id") String veriation_id);
    @FormUrlEncoded
    @POST("update-cart")
    Call<UpdateCartResponse> UpdateCart(@Field("rowId") String rowId, @Field("quantity") String quantity);

    @FormUrlEncoded
    @POST("delete-cart-item")
    Call<CartDeleteResponse> DeleteCart(@Field("rowId") String rowId);

    @FormUrlEncoded
    @POST("cart-count")
    Call<CartCountResponse> CartCount(@Field("token") String token);

    @FormUrlEncoded
    @POST("view-address")
    Call<AddressListResponse> AddressList(@Field("token") String token);

    @FormUrlEncoded
    @POST("my-orders")
    Call<MyOrdersResponse> MyOrders(@Field("token") String token);

    @FormUrlEncoded
    @POST("order-details")
    Call<OrderDetailsResponse> OrderDetails(@Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("add-address")
    Call<AddAddressResponse> AddAddress(@Field("token") String token, @Field("first_name") String first_name, @Field("last_name") String last_name,
                                        @Field("email") String email, @Field("phone") String phone, @Field("pin_code") String pin_code,
                                        @Field("address_line1") String address_line1, @Field("address_line2") String address_line2, @Field("city") String city,
                                        @Field("area") String area, @Field("country") String country, @Field("state") String state, @Field("alternate_phone") String alternate_phone,
                                        @Field("defaults") String defaults);

    @FormUrlEncoded
    @POST("update-address")
    Call<UpdateAddressResponse> UpdateAddress(@Field("token") String token, @Field("first_name") String first_name, @Field("last_name") String last_name,
                                              @Field("email") String email, @Field("phone") String phone, @Field("pin_code") String pin_code,
                                              @Field("address_line1") String address_line1, @Field("address_line2") String address_line2, @Field("city") String city,
                                              @Field("area") String area, @Field("country") String country, @Field("state") String state, @Field("alternate_phone") String alternate_phone,
                                              @Field("defaults") String defaults, @Field("address_id") String address_id);

    @FormUrlEncoded
    @POST("delete-address")
    Call<DeleteAddressResponse> DeleteAddress(@Field("address_id") String address_id);

    @FormUrlEncoded
    @POST("checkout")
    Call<CheckoutResponse> CheckoutData(@Field("token") String token, @Field("address_id") String address_id);

    @FormUrlEncoded
    @POST("order-place")
    Call<OrderPlaceResponse> OrderPlace(@Field("token") String token, @Field("address_id") String address_id, @Field("payment_method") String payment_method);

    @FormUrlEncoded
    @POST("product-search")
    Call<ProductSearchResponse> ProductSearch(@Field("search") String search);

    @Multipart
    @POST("update-profile")
    Call<UpdateProfileResponse> UpdateProfile(@Part MultipartBody.Part file,
                                              @Part("token") RequestBody token,
                                              @Part("name") RequestBody name,
                                              @Part("phone") RequestBody phone,
                                              @Part ("email") RequestBody email);

    @FormUrlEncoded
    @POST("user-notifications")
    Call<NotificationsResponse> Notifications(@Field("token") String token);


}
