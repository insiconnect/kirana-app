package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.github.thunder413.datetimeutils.DateTimeStyle;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.sample.agrofoods.Activities.OrdersListActivity;
import com.sample.agrofoods.Models.MyOrdersResponse;
import com.sample.agrofoods.R;


import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.MyViewHolder> {
    private Context mContext;
    private List<MyOrdersResponse.DataBean> myOrdersListBeans;

    public MyOrdersAdapter(Context mContext, List<MyOrdersResponse.DataBean> myOrdersListBeans) {
        this.mContext = mContext;
        this.myOrdersListBeans = myOrdersListBeans;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtOrderId, txtOrderstatus,txt_hamount, txt_amount, txtPaymentStatus,txtdd,txtmm,txtyy,
                txtIdTitle,txtOrderTitle,txtpaymentTitle,txtstatus;
        public CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtOrderId = (TextView) itemView.findViewById(R.id.txtOrderId);

            txtPaymentStatus=(TextView)itemView.findViewById(R.id.txtPaymentStatus);
            txtOrderstatus=(TextView)itemView.findViewById(R.id.txtOrderstatus);
            txtstatus=(TextView)itemView.findViewById(R.id.txtstatus);

            txt_hamount=(TextView)itemView.findViewById(R.id.txt_hamount);
            txt_amount=(TextView)itemView.findViewById(R.id.txt_amount);

            txtdd=(TextView)itemView.findViewById(R.id.txtdd);
            txtmm=(TextView)itemView.findViewById(R.id.txtmm);
            txtyy=(TextView)itemView.findViewById(R.id.txtyy);

            txtOrderTitle=(TextView)itemView.findViewById(R.id.txtOrderTitle);
            txtpaymentTitle=(TextView)itemView.findViewById(R.id.txtpaymentTitle);
            cardView=(CardView) itemView.findViewById(R.id.cardView);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.orderidlayout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
      //  final MyOrdersResponse.DataBean.RecordDataBean orderListBean = myOrdersListBeans.get(position);

        setFadeAnimation(holder.itemView);

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate rdom color
        int color1 = generator.getRandomColor();

        holder.cardView.setCardBackgroundColor(color1);

        holder.txtOrderTitle.setText("ORDER ID : ");
        holder.txtpaymentTitle.setText("Payment Type : ");
        holder.txtstatus.setText("ORDER Status : ");
        holder.txt_hamount.setText("ORDER Amount : ");
        holder.txtOrderId.setText( myOrdersListBeans.get(position).getOrder_id());
        holder.txtOrderstatus.setText( myOrdersListBeans.get(position).getStatus());
        holder.txt_amount.setText( myOrdersListBeans.get(position).getGrand_total());

        if (myOrdersListBeans.get(position).getPayment_status() == null){
            holder.txtPaymentStatus.setText("");
        }
        else {
            holder.txtPaymentStatus.setText(myOrdersListBeans.get(position).getPayment_method());
        }



        Log.d("TIMEZONE","String To Date >> "+ DateTimeUtils.formatDate(myOrdersListBeans.get(position).getOrder_date()));

        Date date = DateTimeUtils.formatDate(myOrdersListBeans.get(position).getOrder_date());
        String fullDate = DateTimeUtils.formatWithStyle(date, DateTimeStyle.MEDIUM); // June 13, 2017

        StringTokenizer stringTokenizer = new StringTokenizer(fullDate);
        String monthDate = stringTokenizer.nextToken(",");
        String year = stringTokenizer.nextToken(",");

        StringTokenizer monthdateToken = new StringTokenizer(monthDate);
        String mm = monthdateToken.nextToken(" ");
        String dd = monthdateToken.nextToken(" ");

        Log.e("TOKEN",""+mm+dd);

        holder.txtdd.setText(dd);
        holder.txtmm.setText(mm);
        holder.txtyy.setText(year);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String order_id=myOrdersListBeans.get(position).getOrder_id();

                Intent intent=new Intent(mContext, OrdersListActivity.class);
                intent.putExtra("Order_ID",order_id);
                intent.putExtra("CartStatus", false);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return myOrdersListBeans.size();
    }

    public void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }
}
