package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.util.ColorGenerator;

import com.github.thunder413.datetimeutils.DateTimeStyle;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.sample.agrofoods.Models.NotificationsResponse;
import com.sample.agrofoods.R;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {
    private Context mContext;
    List<NotificationsResponse.DataBean> dataBeanList;

    public NotificationsAdapter(Context context, List<NotificationsResponse.DataBean> dataBeanList) {
        this.mContext=context;
        this.dataBeanList=dataBeanList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_desc,txt_title;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_title = (TextView) itemView.findViewById(R.id.txt_title);

            txt_desc=(TextView)itemView.findViewById(R.id.txt_desc);

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
      //  final MyOrdersResponse.DataBean.RecordDataBean orderListBean = myOrdersListBeans.get(position);

        holder.txt_title.setText(dataBeanList.get(position).getTitle());
        holder.txt_desc.setText(dataBeanList.get(position).getMessage());
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }
}
