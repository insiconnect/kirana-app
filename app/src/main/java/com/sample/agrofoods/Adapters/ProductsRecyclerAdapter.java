package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Activities.ProductDetailsActivity;
import com.sample.agrofoods.Models.AllProductsResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProductsRecyclerAdapter extends RecyclerView.Adapter<ProductsRecyclerAdapter.Holder> {
    List<AllProductsResponse.DataBean> receivePaymentModels;
    Context context;
    public ProductsRecyclerAdapter(Context context, List<AllProductsResponse.DataBean> receivePaymentModels) {
        this.context=context;
        this.receivePaymentModels=receivePaymentModels;
    }

    @NonNull
    @Override
    public ProductsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.products_item_card, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsRecyclerAdapter.Holder holder, int position) {

        holder.txtproductName.setText(receivePaymentModels.get(position).getName());
        Picasso.get().load(receivePaymentModels.get(position).getImage()).error(R.drawable.placeholder).into(holder.thumbnail);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pid=String.valueOf(receivePaymentModels.get(position).getId());
                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("productId",pid);
                intent.putExtra("productName",receivePaymentModels.get(position).getName());
                intent.putExtra("status","listing");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return receivePaymentModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        ImageView thumbnail;
        TextView txtproductName;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txtproductName=itemView.findViewById(R.id.txtproductName);
            thumbnail=itemView.findViewById(R.id.thumbnail);

        }
    }
}
