package com.sample.agrofoods.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import com.google.android.material.snackbar.Snackbar;
import com.sample.agrofoods.Activities.AddressListActivity;
import com.sample.agrofoods.Activities.CheckoutActivity;
import com.sample.agrofoods.Activities.UpdateAddressActivity;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.Models.AddressModelItem;
import com.sample.agrofoods.Models.DeleteAddressResponse;
import com.sample.agrofoods.R;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sample.agrofoods.Utilities.capitalize;


public class AddressAdapter extends BaseAdapter {

    private int selectedIndex = -1;
    private Context mContext;
    private List<AddressModelItem> cartListBeanList;
    private String tokenValue;
    private String userId;
    private boolean checkoutStatus;

    public AddressAdapter(Context mContext, List<AddressModelItem> cartListBeanList, boolean checkoutStatus) {
        this.mContext = mContext;
        this.cartListBeanList = cartListBeanList;
        this.checkoutStatus = checkoutStatus;
    }

    @Override
    public int getCount() {
        return cartListBeanList.size();
    }

    @Override
    public Object getItem(int i) {
        return cartListBeanList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View view1 = View.inflate(mContext, R.layout.address_card, null);


        final AddressModelItem cartListBean = cartListBeanList.get(i);

        Log.d("ADDID", "getView: " + cartListBean.getId());

        TextView txtName, txtAddress, txtMobile;
        Button btnEdit, btnDelete;
        RadioButton radioButton;
        Button btnDelivery;


        txtName = (TextView) view1.findViewById(R.id.txtName);
        txtAddress = (TextView) view1.findViewById(R.id.txtAddress);
        txtMobile = (TextView) view1.findViewById(R.id.txtMobile);
        btnEdit = (Button) view1.findViewById(R.id.txtEdit);
        btnDelete = (Button) view1.findViewById(R.id.btnDelete);
        radioButton = (RadioButton) view1.findViewById(R.id.radioButton);
        btnDelivery = (Button) view1.findViewById(R.id.btnDelivery);


        txtName.setText(capitalize(cartListBean.getName()));
        txtAddress.setText(capitalize(cartListBean.getAddress_line1() + "," + cartListBean.getAddress_line2() + "," + cartListBean.getArea() + "," + cartListBean.getCity() + "," +
                cartListBean.getState() + "," + cartListBean.getPincode()));

        if (cartListBean.getAlternate_contact_no() == null || cartListBean.getAlternate_contact_no().equals("")) {
            txtMobile.setText("Mobile : " + cartListBean.getContact_no());

        } else {
            txtMobile.setText("Mobile : " + cartListBean.getContact_no() + "\n" + "Alternate Contact no : " + cartListBean.getAlternate_contact_no());

        }

        radioButton.setVisibility(View.GONE);


        if (selectedIndex == i) {

            radioButton.setChecked(true);
            btnEdit.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.VISIBLE);


            if (checkoutStatus) {
                btnDelivery.setVisibility(View.VISIBLE);
            } else {
                btnDelivery.setVisibility(View.GONE);
            }


        } else {
            radioButton.setChecked(false);
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
            btnDelivery.setVisibility(View.GONE);
        }


        btnDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = (Activity) mContext;
                Intent intent = new Intent(mContext, CheckoutActivity.class);
                intent.putExtra("addressId", cartListBean.getId());
                intent.putExtra("Checkout", checkoutStatus);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                //activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity activity = (Activity) mContext;
                Intent intent =new Intent(mContext, UpdateAddressActivity.class);
                intent.putExtra("addressid",cartListBean.getId());
                intent.putExtra("city",cartListBean.getCity());
                intent.putExtra("username",cartListBean.getName());
                intent.putExtra("state",cartListBean.getState());
                intent.putExtra("mobile",cartListBean.getContact_no());
                intent.putExtra("addressline1",cartListBean.getAddress_line1());
                intent.putExtra("addressline2",cartListBean.getAddress_line2());
                intent.putExtra("area",cartListBean.getArea());
                intent.putExtra("pincode",cartListBean.getPincode());
                intent.putExtra("altenateno",cartListBean.getAlternate_contact_no());
                intent.putExtra("setdefault",cartListBean.getIs_default());
                intent.putExtra("Checkout", checkoutStatus);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
//                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                // Toast.makeText(mContext,""+cartListBean.getAddressId(),Toast.LENGTH_SHORT).show();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.alert_dialog1);


                // set the custom dialog components - text, image and button
                TextView te=(TextView)dialog.findViewById(R.id.txtAlert);
                te.setText("Are You Sure Want to Delete ?");

                TextView yes=(TextView) dialog.findViewById(R.id.btnYes);
                TextView no=(TextView) dialog.findViewById(R.id.btnNo);

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final AddressModelItem cartListBean = cartListBeanList.get(i);

                        delete(v,cartListBean.getId());
                        cartListBeanList.remove(i);
                        notifyDataSetChanged();
                        Log.e("COUNTADDRESS",""+cartListBeanList.size());

                       /* if (cartListBeanList.size()==0){
                            Intent intent = new Intent(mContext, AddressListActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("Checkout", checkoutStatus);
                            mContext.startActivity(intent);
                        }*/

                        dialog.dismiss();


                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        return view1;

    }

    public void setSelectedIndex(int index) {
        selectedIndex = index;

    }


    private void delete(final View view, String id) {

        Call<DeleteAddressResponse> call= RetrofitClient.getInstance().getApi().DeleteAddress(id);
        call.enqueue(new Callback<DeleteAddressResponse>() {
            @Override
            public void onResponse(Call<DeleteAddressResponse> call, Response<DeleteAddressResponse> response) {
                if (response.isSuccessful());
                DeleteAddressResponse deleteAddressResponse=response.body();
                if (deleteAddressResponse.getCode() == 200){
                    Snackbar.make(view, "Address Deleted", Snackbar.LENGTH_SHORT).show();

                    Intent intent = new Intent(mContext, AddressListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("Checkout", checkoutStatus);
                    mContext.startActivity(intent);
                }
                else if (deleteAddressResponse.getCode() == 401){
                    Snackbar.make(view, Html.fromHtml("<font color=\""+Color.RED+"\">"+ "Not Found!"+"</font>"),Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeleteAddressResponse> call, Throwable t) {
                Snackbar.make(view, Html.fromHtml("<font color=\""+Color.RED+"\">"+ "Server Error.."+"</font>"),Snackbar.LENGTH_SHORT).show();

            }
        });

    }
}