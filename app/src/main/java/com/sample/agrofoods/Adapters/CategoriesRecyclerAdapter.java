package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.sample.agrofoods.Activities.GroceryHomeActivity1;
import com.sample.agrofoods.Activities.ProductListActivity;
import com.sample.agrofoods.Models.HomeDataResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;



public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.Holder> {
    List<HomeDataResponse.DataBean.CategoriesBean> receivePaymentModels;
    Context context;
    public CategoriesRecyclerAdapter(Context context, List<HomeDataResponse.DataBean.CategoriesBean> receivePaymentModels) {
        this.context=context;
        this.receivePaymentModels=receivePaymentModels;
    }

    @NonNull
    @Override
    public CategoriesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_categories, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesRecyclerAdapter.Holder holder, int position) {

        holder.txt_name.setText(receivePaymentModels.get(position).getName());
        Picasso.get().load(receivePaymentModels.get(position).getImage()).error(R.drawable.placeholder).into(holder.image_category);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(context, CategoriesActivity.class);
//                context.startActivity(intent);

                if (receivePaymentModels.get(position).getSub_categorie() == 1){
                    Intent intent = new Intent(context, GroceryHomeActivity1.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("CatId", String.valueOf(receivePaymentModels.get(position).getId()));
                    intent.putExtra("Cat_name",receivePaymentModels.get(position).getName());
                    context.startActivity(intent);
                }
                else {
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.putExtra("catId", String.valueOf(receivePaymentModels.get(position).getId()));
                    intent.putExtra("title", receivePaymentModels.get(position).getName());
                    intent.putExtra("subcatId","");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return receivePaymentModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        CircleImageView image_category;
        TextView txt_name;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_name=itemView.findViewById(R.id.txt_name);
            image_category=itemView.findViewById(R.id.image_category);

        }
    }
}
