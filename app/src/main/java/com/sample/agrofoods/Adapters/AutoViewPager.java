package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.demono.adapter.InfinitePagerAdapter;
import com.sample.agrofoods.Models.HomeDataResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;


import java.util.List;




public class AutoViewPager extends InfinitePagerAdapter {

    private List<HomeDataResponse.DataBean.BannersBean> data;
    private Context context;

    public AutoViewPager(List<HomeDataResponse.DataBean.BannersBean> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup container) {
        //BannersResponse.DataBean homeBannersBean = data.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.banner_card, container, false);
        }
        // Lookup view for data population
        TextView tvDiscription = (TextView) convertView.findViewById(R.id.txtDescription);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        // Populate the data into the template view using the data object
        tvDiscription.setText(data.get(position).getName());
        Picasso.get().load(data.get(position).getImage()).error(R.drawable.placeholder).into(imageView);
    //    Glide.with(context).load(IMAGE_BASE_URL+ homeBannersBean.getImage()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(imageView);
        // Return the completed view to render on screen
        return convertView;
    }
}
