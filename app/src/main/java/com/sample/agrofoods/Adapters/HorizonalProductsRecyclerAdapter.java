package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Models.AllProductsResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HorizonalProductsRecyclerAdapter extends RecyclerView.Adapter<HorizonalProductsRecyclerAdapter.Holder> {
    Context context;
    List<AllProductsResponse.DataBean> productsResponses;

    public HorizonalProductsRecyclerAdapter(Context context, List<AllProductsResponse.DataBean> productsBeanList) {
        this.context=context;
        this.productsResponses=productsBeanList;
    }

    @NonNull
    @Override
    public HorizonalProductsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_horizontal, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HorizonalProductsRecyclerAdapter.Holder holder, int position) {
      //  holder.product_price.setText(productsResponses[position].getP_price());
        holder.p_name.setText(productsResponses.get(position).getName());
        Picasso.get().load(productsResponses.get(position).getImage()).into(holder.p_image);
    }

    @Override
    public int getItemCount() {
        return productsResponses.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        ImageView p_image;
        TextView p_name,product_price;

        public Holder(@NonNull View itemView) {
            super(itemView);
            p_image=itemView.findViewById(R.id.p_image);
            p_name=itemView.findViewById(R.id.p_name);
            product_price=itemView.findViewById(R.id.product_price);

        }
    }
}
