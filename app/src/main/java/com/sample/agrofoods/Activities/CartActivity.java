package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.sample.agrofoods.Adapters.CartRecyclerAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.CartProductClickListener;
import com.sample.agrofoods.Models.CartDeleteResponse;
import com.sample.agrofoods.Models.CartListingResponse;
import com.sample.agrofoods.Models.Product;
import com.sample.agrofoods.Models.UpdateCartResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity implements CartProductClickListener {

    @BindView(R.id.productsRcycler)
    RecyclerView productsRcycler;
    @BindView(R.id.txtSubTotal)
    TextView txtSubTotal;
    @BindView(R.id.txtSave)
    TextView txtSave;

    @BindView(R.id.btnCheckOut)
    Button btnCheckOut;
    @BindView(R.id.bottamlayout)
    LinearLayout bottamlayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;
    AppController appController;
    @BindView(R.id.txtAlert)
    TextView txtAlert;
    PrefManager prefManager;

    String token, tokenValue, deviceId;
    //int totalQty = 0;
    int totalQuantity = 0;
    int minimumOrder = 500;

    public String length;
    public String module;
    CartProductClickListener cartProductClickListener;
    CartRecyclerAdapter cartAdapter;

    ArrayList<Product> productArrayList;
    @BindView(R.id.img_back)
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.buttonbackground));
        //  getSupportActionBar().setTitle("Cart");
        cartProductClickListener = (CartProductClickListener) this;
        //  cartProductClickListener = (CartProductClickListener) this;

        appController = (AppController) getApplication();

        prefManager = new PrefManager(CartActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        // Displaying user information from shared preferences
        //  deviceId = profile.get("deviceId");

        if (appController.isConnection()) {
            //   appController.cartCount(token);
            cartData(token);

        } else {

            Toast.makeText(this, "No Internet Connection!..", Toast.LENGTH_SHORT).show();
            // app.internetDilogue(KitchenitemListActivity.this);

        }

    }

    @Override
    public void onMinusClick(Product cartItemsBean) {
        int i = productArrayList.indexOf(cartItemsBean);

        if (cartItemsBean.getPurchase_quantity() > 1) {
            Product updatedProduct = new Product(cartItemsBean.getId(), cartItemsBean.getProduct_id(), cartItemsBean.getProduct_name(), cartItemsBean.getMrp_price(), (cartItemsBean.getPurchase_quantity() - 1), cartItemsBean.getImages(), cartItemsBean.getDiscount_price(), cartItemsBean.getQuantity(),String.valueOf(cartItemsBean.getProfit()));

            productArrayList.remove(cartItemsBean);
            productArrayList.add(i, updatedProduct);

            updateQuantity(updatedProduct);
            cartAdapter.notifyDataSetChanged();

            calculateCartTotal();

        }
    }

    @Override
    public void onPlusClick(Product cartItemsBean) {

        int i = productArrayList.indexOf(cartItemsBean);
        Product updatedProduct = new Product(cartItemsBean.getId(), cartItemsBean.getProduct_id(), cartItemsBean.getProduct_name(), cartItemsBean.getMrp_price(), (cartItemsBean.getPurchase_quantity() + 1), cartItemsBean.getImages(), cartItemsBean.getDiscount_price(), cartItemsBean.getQuantity(),String.valueOf(cartItemsBean.getProfit()));

        productArrayList.remove(cartItemsBean);
        productArrayList.add(i, updatedProduct);

        updateQuantity(updatedProduct);
        cartAdapter.notifyDataSetChanged();

        calculateCartTotal();
    }

    @Override
    public void onSaveClick(Product product) {

    }

    @Override
    public void onRemoveDialog(Product product) {

        SweetAlertDialog sd = new SweetAlertDialog(CartActivity.this, SweetAlertDialog.WARNING_TYPE);
        sd.setTitleText("Are you sure?");
        sd.setContentText("This product will be delete from cart!");
        sd.setCancelText("No,cancel pls!");
        sd.setConfirmText("Yes,delete it!");
        sd.showCancelButton(true);
//                    sd.getProgressHelper().setBarColor(Color.parseColor("#2596c8"));
        sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(final SweetAlertDialog sweetAlertDialog) {

                //cart_id = String.valueOf(receivePaymentModels.get(position).getId());
                delete(product);
                sweetAlertDialog.dismiss();
                cartAdapter.notifyDataSetChanged();
                Snackbar.make(parentLayout, "Deleted Successfully", Snackbar.LENGTH_SHORT).show();
            }
        });

        sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.cancel();
            }
        });
        sd.show();

    }


    private void cartData(String token) {

        productArrayList = new ArrayList<>();
        ProgressDialog progressDialog = new ProgressDialog(CartActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<CartListingResponse> call = RetrofitClient.getInstance().getApi().CartListing(token);
        call.enqueue(new Callback<CartListingResponse>() {
            @Override
            public void onResponse(Call<CartListingResponse> call, Response<CartListingResponse> response) {
                if (response.isSuccessful()) ;
                CartListingResponse cartListingResponse = response.body();
                if (cartListingResponse.getCode() == 200) {
                    progressDialog.dismiss();
                    assert cartListingResponse != null;
                    for (CartListingResponse.DataBean dataBean : cartListingResponse.getData()) {

                        productArrayList.add(new Product(dataBean.getRowId(), dataBean.getId(), dataBean.getName(),
                                dataBean.getPrice(), Integer.parseInt(dataBean.getQty()), dataBean.getImage(), dataBean.getDiscount(), dataBean.getOptions().getQuantity(),String.valueOf(dataBean.getProfit())));
                    }

                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    productsRcycler.setLayoutManager(mLayoutManager1);
                    productsRcycler.setItemAnimator(new DefaultItemAnimator());
                    productsRcycler.setHasFixedSize(true);

                    cartAdapter = new CartRecyclerAdapter(CartActivity.this, productArrayList, cartProductClickListener);
                    productsRcycler.setAdapter(cartAdapter);
                    cartAdapter.notifyDataSetChanged();
                    // calculate total amount
                    calculateCartTotal();
                } else if (cartListingResponse.getCode() == 204) {
                    progressDialog.dismiss();
                    // Toast.makeText(CartActivity.this, "No Data!...", Toast.LENGTH_SHORT).show();

                    setContentView(R.layout.emptycart);
                    Button btnGotohome = (Button) findViewById(R.id.btnGotohome);
                    btnGotohome.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(CartActivity.this, DashBoard.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            //  overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    });


                }
            }

            @Override
            public void onFailure(Call<CartListingResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CartActivity.this, "Server Error..", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void updateQuantity(Product updatedProduct) {

        Call<UpdateCartResponse> call = RetrofitClient.getInstance().getApi().UpdateCart(updatedProduct.getId(), String.valueOf(updatedProduct.getPurchase_quantity()));
        call.enqueue(new Callback<UpdateCartResponse>() {
            @Override
            public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {
                if (response.isSuccessful()) ;
                UpdateCartResponse updateCartResponse = response.body();

                if (updateCartResponse.getCode() == 200) {

                } else if (updateCartResponse.getCode() == 401) {

                }
            }

            @Override
            public void onFailure(Call<UpdateCartResponse> call, Throwable t) {
                Toast.makeText(CartActivity.this, "Server Error..", Toast.LENGTH_SHORT).show();

            }
        });

    }

    // total Amount
    public void calculateCartTotal() {

        int grandTotal = 0;
        int grandsave = 0;
        double grandsave1 = 0;

        for (Product order : productArrayList) {

            grandTotal += (ParseDouble(order.getDiscount_price()) * order.getPurchase_quantity());
            grandsave += (ParseDouble(order.getMrp_price()) - (ParseDouble(order.getDiscount_price())));
            grandsave1 += Double.parseDouble(order.getMrp_price()) - Double.parseDouble(order.getDiscount_price());

        }

        Log.e("TOTAL", "" + productArrayList.size() + "Items | " + "  DISCOUNT : " + grandTotal);

        txtSubTotal.setText(productArrayList.size() + " Items | Rs " + grandTotal);
        String finalvalue=Double.toString(grandsave1);

        txtSave.setText(" saved : "+" Rs " + grandsave);

        if (grandTotal >= minimumOrder) {

            btnCheckOut.setVisibility(View.VISIBLE);
            txtAlert.setVisibility(View.GONE);
        } else {

            btnCheckOut.setVisibility(View.INVISIBLE);
            txtAlert.setVisibility(View.VISIBLE);
            txtAlert.setText("Minimum Order Must Be Greater Than Rs." + minimumOrder + "*");

        }

        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout", true);
                startActivity(intent);
                //  overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        if (productArrayList.size() == 0) {

            setContentView(R.layout.emptycart);
            Button btnGotohome = (Button) findViewById(R.id.btnGotohome);
            btnGotohome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(CartActivity.this, DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    //    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

    }

    private void delete(Product product) {

        Call<CartDeleteResponse> call = RetrofitClient.getInstance().getApi().DeleteCart(product.getId());
        call.enqueue(new Callback<CartDeleteResponse>() {
            @Override
            public void onResponse(Call<CartDeleteResponse> call, Response<CartDeleteResponse> response) {
                if (response.isSuccessful()) ;
                CartDeleteResponse cartDeleteResponse = response.body();

                if (cartDeleteResponse.getCode() == 200) {
                    int i = productArrayList.indexOf(product);
                    if (i == -1) {
                        throw new IndexOutOfBoundsException();
                    }
                    productArrayList.remove(productArrayList.get(i));

                    // cart count Update
                    //  appController.cartCount(userId, deviceId);
                    invalidateOptionsMenu();

                    calculateCartTotal();
                    cartAdapter.notifyDataSetChanged();
                } else if (cartDeleteResponse.getCode() == 401) {
                    Toast.makeText(CartActivity.this, cartDeleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartDeleteResponse> call, Throwable t) {
                Toast.makeText(CartActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();

            }
        });

    }


    private double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        // cart count Update
        //  appController.cartCount(token);

        cartData(token);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // cart count Update
        //appController.cartCount(token);

    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//
//            case android.R.id.home:
//                invalidateOptionsMenu();
//                onBackPressed();
//
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }



    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


}
