package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.textfield.TextInputEditText;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.LoginResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends BaseActivity implements View.OnClickListener {

    Button otp,btn_continue;
    TextView register;
    TextInputEditText edt_mobile;
    AppController appController;
    PrefManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        appController = (AppController) getApplication();
        session = new PrefManager(this);
        if (session.isLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, DashBoard.class));
            finish();
        }
        otp=findViewById(R.id.otp);
        register=findViewById(R.id.txt_register);
        btn_continue=findViewById(R.id.btn_continue);
        edt_mobile=findViewById(R.id.edt_mobile);
        btn_continue.setOnClickListener(this);
        btn_continue.setAlpha(.5f);
        btn_continue.setEnabled(false);

        edt_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = String.valueOf(charSequence.length());

                if (value.equals("10")) {

                    btn_continue.setEnabled(true);
                    btn_continue.setAlpha(.9f);
                    btn_continue.setText("CONTINUE");
                    btn_continue.setClickable(true);
                } else {
                    btn_continue.setAlpha(.5f);
                    btn_continue.setEnabled(false);
                    btn_continue.setText("ENTER MOBILE NUMBER");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this, VerifyOTPActivity.class);
                startActivity(intent);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        String Mobile = edt_mobile.getText().toString().trim();
        if (Mobile.equals("")) {
            Toast.makeText(LoginActivity.this, "Enter Mobile Number...", Toast.LENGTH_SHORT).show();
        }
        else {

            boolean isConnected = appController.isConnection();
            if (isConnected) {
                getData(Mobile);
            }
            else {
                Toast.makeText(LoginActivity.this, "No Internet Connection!...", Toast.LENGTH_SHORT).show();

            }

        }
    }

    private void getData(String mobile) {
        ProgressDialog progressDialog=new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<LoginResponse> call= RetrofitClient.getInstance().getApi().Login(mobile);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful());
                LoginResponse loginResponse=response.body();
                if (loginResponse.getCode() == 200){
                    progressDialog.dismiss();
                    Intent intent = new Intent(LoginActivity.this, VerifyOTPActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("Mobile_number",mobile);
                    startActivity(intent);
                }
                else if (loginResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (loginResponse.getCode() == 402){
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();

            }
        });



    }
}
