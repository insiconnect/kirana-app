package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;

import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.ProfileResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAccountActivity extends AppCompatActivity {

    @BindView(R.id.txtAccName)
    TextView txtAccName;
    @BindView(R.id.txtAccMobile)
    TextView txtAccMobile;
    @BindView(R.id.txtMail)
    TextView txtMail;
    @BindView(R.id.ChangePassword)
    CardView ChangePassword;
    @BindView(R.id.myAddressCard)
    CardView myAddressCard;
    @BindView(R.id.myOrdersCard)
    CardView myOrdersCard;
    @BindView(R.id.myWhishListCard)
    CardView myWhishListCard;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.parentLayout)
    NestedScrollView parentLayout;
    @BindView(R.id.editProfile)
    ImageView editProfile;
    @BindView(R.id.myNotificationsCard)
    CardView myNotificationsCard;

    @BindView(R.id.img_profile)
    ImageView img_profile;
    PrefManager pref;
    String userId;
    boolean checkoutStatus;
    RelativeLayout txt_logout;
    String image,name,phone,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Account");

        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }

        pref = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = pref.getUserDetails();
        String token = profile.get("Token");
        AppController app = (AppController) getApplication();

        boolean isConnected = app.isConnection();
        if (isConnected) {
            getProfileData(token);
        }
        else {
            setContentView(R.layout.internet);
            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });        }

       // txt_logout=findViewById(R.id.txt_logout);

    }

    private void getProfileData(String token) {

        ProgressDialog progressDialog=new ProgressDialog(MyAccountActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<ProfileResponse> call= RetrofitClient.getInstance().getApi().Profile(token);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful());
                ProfileResponse profileResponse=response.body();

                if (profileResponse.getCode() == 200){
                    progressDialog.dismiss();
                   // Toast.makeText(getApplicationContext(), profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ProfileResponse.DataBean dataBean=profileResponse.getData();
                    name=dataBean.getName();
                    email=dataBean.getEmail();
                    phone=dataBean.getPhone();
                    image=dataBean.getImage();
                    txtAccName.setText(name);
                    txtAccMobile.setText(phone);
                    if (dataBean.getImage() == null || dataBean.getImage().equals("")){
                        img_profile.setImageResource(R.drawable.placeholder);
                    }
                    else {
                        Picasso.get().load(image).error(R.drawable.placeholder).into(img_profile);

                    }

                }
                else if (profileResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(MyAccountActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

                else if (profileResponse.getCode() == 204){
                    progressDialog.dismiss();
                    Toast.makeText(MyAccountActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyAccountActivity.this, "Serer Error....", Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.ChangePassword, R.id.myAddressCard, R.id.myOrdersCard, R.id.myWhishListCard,R.id.myNotificationsCard, R.id.editProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ChangePassword:
//                Intent editMyAcc = new Intent(MyAccountActivity.this, ChangePasswordActivity.class);
//                editMyAcc.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(editMyAcc);

                break;
            case R.id.myAddressCard:
                Intent address = new Intent(MyAccountActivity.this, AddressListActivity.class);
                address.putExtra("Checkout", false);
                address.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(address);

                break;
            case R.id.myOrdersCard:
                Intent orders = new Intent(MyAccountActivity.this, MyOrdersActivity.class);
                orders.putExtra("Checkout", false);
                orders.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(orders);

                break;
            case R.id.myWhishListCard:
//                Intent wishlist = new Intent(MyAccountActivity.this, WishListActivity.class);
//                wishlist.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(wishlist);

                break;
            case R.id.editProfile:
                Intent vechils = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                vechils.putExtra("Name",name);
                vechils.putExtra("Phone",phone);
                vechils.putExtra("Email",email);
                vechils.putExtra("Image",image);

                vechils.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(vechils);

                break;
            case R.id.myNotificationsCard:
//                Intent service = new Intent(MyAccountActivity.this, NotificationActivity.class);
//                service.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(service);

                break;
        }
    }
}
