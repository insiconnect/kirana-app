package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Adapters.SubCategoriesListRecyclerAdapter;
import com.sample.agrofoods.Adapters.SubCategoriesRecyclerAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.AllProductsResponse;
import com.sample.agrofoods.Models.CartCountResponse;
import com.sample.agrofoods.Models.SubCategoriesResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GroceryHomeActivity1 extends AppCompatActivity {
    public static String MODULE = "grocery";
    AppController appController;
    @BindView(R.id.catRecycler)
    RecyclerView catRecycler;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;


    String userid, tokenValue, deviceId;
    int cartindex;
    String catId, cat_name;
    List<AllProductsResponse.DataBean> productsBeanList;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.text_count)
    TextView textCount;
    @BindView(R.id.relative_count)
    RelativeLayout relativeCount;
    private PrefManager prefManager;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_home1);
        ButterKnife.bind(this);
        //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefManager = new PrefManager(GroceryHomeActivity1.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        CartCount(token);
        invalidateOptionsMenu();

        appController = (AppController) getApplication();
        if (getIntent() != null) {
            catId = getIntent().getStringExtra("CatId");
            cat_name = getIntent().getStringExtra("Cat_name");

        }
        txtTitle.setText(cat_name);
        // getSupportActionBar().setTitle(capitalize(cat_name));


        // simpleSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);
        if (appController.isConnection()) {
            CartCount(token);
            SubCatData(catId);
            SubCatlisting(catId);

            // prepareSubCatData();
            // cart count
//            appController.cartCount(token);
//            SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
//            cartindex = preferences.getInt("itemCount", 0);
//            Log.e("cartindex", "" + cartindex);


//            simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//                @Override
//                public void onRefresh() {
//                    SubCatData(catId);
//                    SubCatlisting(catId);
//
//                    ProductListing(catId);
//
//                    simpleSwipeRefreshLayout.setRefreshing(false);
//                }
//            });

        } else {

            setContentView(R.layout.internet);
            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(GroceryHomeActivity1.this, DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }


    }

    private void SubCatData(String catId) {
        ProgressDialog progressDialog = new ProgressDialog(GroceryHomeActivity1.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<SubCategoriesResponse> call = RetrofitClient.getInstance().getApi().Subcategory(catId);
        call.enqueue(new Callback<SubCategoriesResponse>() {
            @Override
            public void onResponse(Call<SubCategoriesResponse> call, Response<SubCategoriesResponse> response) {
                if (response.isSuccessful()) ;
                SubCategoriesResponse subCategoriesResponse = response.body();
                if (subCategoriesResponse.getCode() == 200) {
                    progressDialog.dismiss();
                    List<SubCategoriesResponse.DataBean> dataBeanList = subCategoriesResponse.getData();
                    SubCategoriesRecyclerAdapter subCategoriesRecyclerAdapter = new SubCategoriesRecyclerAdapter(dataBeanList, getApplicationContext());
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    catRecycler.setLayoutManager(mLayoutManager);
                    catRecycler.setItemAnimator(new DefaultItemAnimator());
                    catRecycler.setHasFixedSize(true);
                    catRecycler.setNestedScrollingEnabled(false);
                    catRecycler.setAdapter(subCategoriesRecyclerAdapter);
                } else if (subCategoriesResponse.getCode() == 204) {
                    progressDialog.dismiss();
                    Toast.makeText(GroceryHomeActivity1.this, "No Data...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SubCategoriesResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(GroceryHomeActivity1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void SubCatlisting(String catId) {

        ProgressDialog progressDialog = new ProgressDialog(GroceryHomeActivity1.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<SubCategoriesResponse> call = RetrofitClient.getInstance().getApi().Subcategory(catId);
        call.enqueue(new Callback<SubCategoriesResponse>() {
            @Override
            public void onResponse(Call<SubCategoriesResponse> call, Response<SubCategoriesResponse> response) {
                if (response.isSuccessful()) ;
                SubCategoriesResponse subCategoriesResponse = response.body();
                if (subCategoriesResponse.getCode() == 200) {
                    progressDialog.dismiss();
                    List<SubCategoriesResponse.DataBean> dataBeanList = subCategoriesResponse.getData();
                    SubCategoriesListRecyclerAdapter subCategoriesRecyclerAdapter = new SubCategoriesListRecyclerAdapter(dataBeanList, getApplicationContext(), catId, productsBeanList);
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    myRecyclerView.setLayoutManager(mLayoutManager1);
                    myRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    myRecyclerView.setHasFixedSize(true);
                    myRecyclerView.setNestedScrollingEnabled(false);
                    myRecyclerView.setAdapter(subCategoriesRecyclerAdapter);
                } else if (subCategoriesResponse.getCode() == 204) {
                    progressDialog.dismiss();
                    Toast.makeText(GroceryHomeActivity1.this, "No Data...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SubCategoriesResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(GroceryHomeActivity1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onRestart() {
        CartCount(token);
     //   invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        CartCount(token);
       // invalidateOptionsMenu();
        super.onStart();
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
//        final MenuItem menuItem = menu.findItem(R.id.action_cart);
//        menuItem.setIcon(Converter.convertLayoutToImage(GroceryHomeActivity1.this, cartindex, R.drawable.ic_actionbar_bag));
//        return true;
//    }

    public void CartCount(String token) {

        Call<CartCountResponse> call = RetrofitClient.getInstance().getApi().CartCount(token);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) {
                    CartCountResponse cartCountResponse = response.body();

                    if (cartCountResponse.getCode() == 200) {
                        CartCountResponse.DataBean dataBean = cartCountResponse.getData();
                        cartindex = dataBean.getCount();
                        Log.e("CART_INDEX", "" + cartindex);

                        if (String.valueOf(cartindex).equals("0")) {
                            relativeCount.setVisibility(View.GONE);
                        } else {
                            relativeCount.setVisibility(View.VISIBLE);
                        }

                        textCount.setText(String.valueOf(cartindex));

                    } else if (cartCountResponse.getCode() == 204) {
                        Toast.makeText(getApplicationContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(GroceryHomeActivity1.this, "Server error...", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @OnClick({R.id.img_back, R.id.img_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_cart:
                Intent intent = new Intent(GroceryHomeActivity1.this, CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }

//      @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//
//            case android.R.id.home:
//                onBackPressed();
//                break;
//
//            case R.id.action_cart:
//                Intent intent = new Intent(GroceryHomeActivity1.this, CartActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }


}
