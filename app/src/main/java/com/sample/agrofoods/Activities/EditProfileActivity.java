package com.sample.agrofoods.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.GalleryUriToPath;
import com.sample.agrofoods.Models.ProfileResponse;
import com.sample.agrofoods.Models.UpdateProfileResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {
    @BindView(R.id.etUsername)
    TextInputEditText etUsername;
    @BindView(R.id.user_til)
    TextInputLayout userTil;
    @BindView(R.id.etPhone)
    TextInputEditText etPhone;
    @BindView(R.id.mobile_til)
    TextInputLayout mobileTil;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.email_til)
    TextInputLayout emailTil;

    @BindView(R.id.btnUpdate)
    Button btnUpdate;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.p_img)
    CircleImageView p_img;

    @BindView(R.id.relative1)
    RelativeLayout relative1;
    private PrefManager pref;

    ImageView image;

    String Token,tokenValue,deviceId,gender;
    AppController app;
    public static final int MY_REQUEST_CODE = 1;
    int PICK_IMAGE = 2;
    String picked_image = "empty";
    int CAMERA_CAPTURE = 1;
    File file = null;
    private ArrayList<Image> images;
    String name,email,phone,imagee;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Update Profile");

        app = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());

        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        Token = profile.get("Token");

        if (getIntent() != null){
            name=getIntent().getStringExtra("Name");
            email=getIntent().getStringExtra("Email");
            phone=getIntent().getStringExtra("Phone");
            imagee=getIntent().getStringExtra("Image");

            etUsername.setText(name);
            etPhone.setText(phone);
            etEmail.setText(email);

            if (imagee == null || imagee.equals("")){
                p_img.setImageResource(R.drawable.placeholder);
            }
            else {
                Picasso.get().load(imagee).error(R.drawable.placeholder).into(p_img);
            }

        }

//        etUsername.addTextChangedListener(new MyTextWatcher(etUsername));
//        etPhone.addTextChangedListener(new MyTextWatcher(etPhone));
//        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));

        relative1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissions();
//                ImagePicker.with(EditProfileActivity.this)                         //  Initialize ImagePicker with activity or fragment context
//                        .setToolbarColor("#212121")         //  Toolbar color
//                        .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
//                        .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
//                        .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
//                        .setProgressBarColor("#4CAF50")     //  ProgressBar color
//                        .setBackgroundColor("#212121")      //  Background color
//                        .setCameraOnly(false)               //  Camera mode
//                        .setMultipleMode(false)              //  Select multiple images or single image
//                        .setFolderMode(true)                //  Folder mode
//                        .setShowCamera(true)                //  Show camera button
//                        .setFolderTitle("Albums")           //  Folder title (works with FolderMode = true)
//                        .setImageTitle("Galleries")         //  Image title (works with FolderMode = false)
//                        .setDoneTitle("Done")               //  Done button title
//                        .setLimitMessage("You have reached selection limit")    // Selection limit message
//                        .setMaxSize(10)                     //  Max images can be selected
//                        .setSavePath("ImagePicker")         //  Image capture folder name
//                        .setSelectedImages(images)          //  Selected images
//                        .setAlwaysShowDoneButton(true)      //  Set always show done button in multiple mode
//                        .setKeepScreenOn(true)              //  Keep screen on when selecting images
//                        .start();                           //  Start ImagePicker

            }
        });

    }

    private void permissions() {
        if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_REQUEST_CODE);
        } else {
            select_image();
        }

    }

    private void select_image() {

        final CharSequence[] options = {getResources().getString(R.string.takeaphoto), getResources().getString(R.string.choose_gallery),getResources().getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle(getResources().getString(R.string.photos));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.choose_gallery))) {
                    choosefromgallery();

                } else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }

            }
        });

        builder.show();
    }

    private void choosefromgallery() {
        try {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(galleryIntent, "Select Picture"), PICK_IMAGE);
           /* Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE);*/
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.btnUpdate)
    public void onViewClicked() {
        boolean isConnected = app.isConnection();
        if (isConnected) {
            updateProfile();

        } else {

            String message = "Sorry! Not connected to internet";
            int color = Color.RED;
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }

    private void profileData(String token) {
        Call<ProfileResponse> call= RetrofitClient.getInstance().getApi().Profile(token);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful());
                ProfileResponse profileResponse=response.body();

                if (profileResponse.getCode() == 200){
                  //  progressDialog.dismiss();
                 //   Toast.makeText(getContext(), profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ProfileResponse.DataBean dataBean=profileResponse.getData();
                    etUsername.setText(dataBean.getName());
                    etEmail.setText(dataBean.getEmail());
                    etPhone.setText(dataBean.getPhone());

                    if (dataBean.getImage() == null){
                        p_img.setImageResource(R.drawable.placeholder);
                    }
                    else {
                        Picasso.get().load(dataBean.getImage()).error(R.drawable.placeholder).into(p_img);
                    }

                }
                else if (profileResponse.getCode() == 401){
                  //  progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
             //   progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Serer Error....", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void updateProfile() {
        String MobilePattern = "[0-9]{10}";
        final String name = etUsername.getText().toString().trim();
        final String mobile = etPhone.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();

//        if ((!isValidName(name))) {
//            return;
//        }
//        if ((!isValidPhoneNumber(mobile))) {
//            return;
//        }
//        if ((!isValidEmail(email))) {
//            return;
//        }
//
//        btnUpdate.setEnabled(false);

        if (name.equals("")){

            Toast.makeText(this, "Enter Valid name", Toast.LENGTH_SHORT).show();
        }
        else if (!mobile.matches(MobilePattern)){
            Toast.makeText(this, "Enter Valid Mobile number", Toast.LENGTH_SHORT).show();

        }
        else if (!isEmailValid(email) || email.isEmpty()){
            Toast.makeText(this, "Enter Valid email", Toast.LENGTH_SHORT).show();

        }
        else {

            ProgressDialog progressDialog=new ProgressDialog(EditProfileActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();


            RequestBody firstnam = RequestBody.create(MediaType.parse("multipart/form-data"), name);
            RequestBody token = RequestBody.create(MediaType.parse("multipart/form-data"), Token);

            RequestBody Email = RequestBody.create(MediaType.parse("multipart/form-data"), email);
            RequestBody Mobile = RequestBody.create(MediaType.parse("multipart/form-data"), mobile);

            MultipartBody.Part image_profile = null;
            if (!picked_image.equals("empty")) {
                file = new File(picked_image);
                RequestBody requestBody = RequestBody.create(MediaType.parse(getMimeType(picked_image)), file);
                image_profile = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
                Log.d("image", ">>>>>>>>>>" + image_profile);
                Log.e("alm", picked_image);
                //preferenceUtils.saveString(PreferenceUtils.IMAGE,PickedImgPath);
            }

//            MultipartBody.Part body = null;
//            if (file != null) {
//                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//                body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
//
//            }

            Call<UpdateProfileResponse> call=RetrofitClient.getInstance().getApi().UpdateProfile(image_profile,token,firstnam,Mobile,Email);
            call.enqueue(new Callback<UpdateProfileResponse>() {
                @Override
                public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                    if (response.isSuccessful());
                    UpdateProfileResponse updateProfileResponse=response.body();
                    if (updateProfileResponse.getCode() == 200){
                        progressDialog.dismiss();
                        Toast.makeText(EditProfileActivity.this, updateProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (updateProfileResponse.getCode() == 401){
                        progressDialog.dismiss();
                        Toast.makeText(EditProfileActivity.this, updateProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(EditProfileActivity.this, "Server Error..", Toast.LENGTH_SHORT).show();

                }
            });

        }


    }
    // validate name
    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            userTil.setError("name is required");
            requestFocus(etUsername);
            return false;
        } else if (!matcher.matches()) {
            userTil.setError("Enter Alphabets Only");
            requestFocus(etUsername);
            return false;
        } else if (name.length() < 5 || name.length() > 20) {
            userTil.setError("Name Should be 5 to 20 characters");
            requestFocus(etUsername);
            return false;
        } else {
            userTil.setErrorEnabled(false);
        }
        return matcher.matches();
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTil.setError("Phone no is required");
            requestFocus(etPhone);

            return false;
        } else if (!matcher.matches()) {
            mobileTil.setError("Enter a valid mobile");
            requestFocus(etPhone);
            return false;
        } else {
            mobileTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }


    // validate your email address
    private boolean isValidEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        if (email.isEmpty()) {
            emailTil.setError("Email is required");
            requestFocus(etEmail);
            return false;
        } else if (!matcher.matches()) {
            emailTil.setError("Enter a valid email");
            requestFocus(etEmail);
            return false;
        } else {
            emailTil.setErrorEnabled(false);
        }


        return matcher.matches();
    }


    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    // text input layout class
//    private class MyTextWatcher implements TextWatcher {
//
//        private View view;
//
//        private MyTextWatcher(View view) {
//            this.view = view;
//        }
//
//        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        public void afterTextChanged(Editable editable) {
//            switch (view.getId()) {
//                case R.id.etUsername:
//                    isValidName(etUsername.getText().toString().trim());
//                    break;
//                case R.id.etPhone:
//                    isValidPhoneNumber(etPhone.getText().toString().trim());
//                    break;
//
//                case R.id.etEmail:
//                    isValidEmail(etEmail.getText().toString().trim());
//                    break;
//
//            }
//        }
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

//        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
//            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
//
//            try {
//                if (images != null) {
//                    for (int i = 0; i < images.size(); i++) {
//                        Uri uri = Uri.fromFile(new File(images.get(0).getPath()));
//                        Picasso.get().load(uri).into(p_img);
////                        Picasso.with(getApplicationContext()).load(uri).into(profile_image);
//                        file = null;
//                        String filepath = images.get(0).getPath();
//                        if (filepath != null && !filepath.equals("")) {
//                            file = new File(filepath);
//                        }
//                    }
//                }
//            } catch (NullPointerException e) {
//                e.printStackTrace();
//            }
//        }
        super.onActivityResult(requestCode, resultCode, data);

    //    super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
               // onCaptureImageResult(data);
            }
        } else if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                //  Picked_profile = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                picked_image = GalleryUriToPath.getPath(getApplicationContext(), picUri);
                String file_profile = picked_image.substring(picked_image.lastIndexOf("/") + 1);
                //txt_image_only.setText(filename);
                //  txt_profile_photo.setText(file_profile);
                try {
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
                    p_img.setImageBitmap(bm);
                 //   u_img.setVisibility(View.GONE);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                //  String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);
                // txt_image_only.setText("photo Id has selected");
                c.close();
            }
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        Log.d("MIME_TYPE_EXT", extension);
        if (extension != null && extension != "") {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            //  Log.d("MIME_TYPE", type);
        } else {
            FileNameMap fileNameMap = URLConnection.getFileNameMap();
            type = fileNameMap.getContentTypeFor(url);
        }
        return type;
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

}
