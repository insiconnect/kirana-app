package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.RegistrationResponse;
import com.sample.agrofoods.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegistrationActivity extends AppCompatActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.mobile_edt)
    TextInputEditText mobileEdt;
    @BindView(R.id.name_edt)
    TextInputEditText nameEdt;
    @BindView(R.id.email_edt)
    TextInputEditText emailEdt;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    AppController appController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        appController = (AppController) getApplicationContext();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @OnClick({R.id.back, R.id.btn_signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
//                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
//                startActivity(intent);
                break;
            case R.id.btn_signup:
                if (appController.isConnection()){
                    String mobile = mobileEdt.getText().toString();
                    String name = nameEdt.getText().toString();
                    String email = emailEdt.getText().toString();

                    if (name.isEmpty()) {
                        nameEdt.setError("Enter Your Name");
                        return;
                    }
                    else if (email.isEmpty()) {
                        emailEdt.setError("Enter Your Email");
                        return;
                    }
                    else if (mobile.equals("")){
                        mobileEdt.setError("Enter Your Mobile Number");
                    }
                    else {
                        userRegistration(name, email, mobile);
                    }
                }
                else {

                    Toast.makeText(this, "No Internet Connection!...", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    private void userRegistration(String name, String email, String mobile) {
        ProgressDialog progressDialog = new ProgressDialog(RegistrationActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        Call<RegistrationResponse> call= RetrofitClient.getInstance().getApi().Registration(mobile,name,email);
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                if (response.isSuccessful());
                RegistrationResponse registrationResponse=response.body();

                if (registrationResponse.getCode() == 200){
                    progressDialog.dismiss();
                    Toast.makeText(RegistrationActivity.this, registrationResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegistrationActivity.this, VerifyOTPActivity.class);
                    intent.putExtra("Mobile_number", mobile);
                    startActivity(intent);
                }
                else if (registrationResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(RegistrationActivity.this, registrationResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (registrationResponse.getCode() == 409){
                    progressDialog.dismiss();
                    Toast.makeText(RegistrationActivity.this, registrationResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(RegistrationActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
