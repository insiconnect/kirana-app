package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.sample.agrofoods.Adapters.SpinnerAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.AddCartResponse;
import com.sample.agrofoods.Models.CartCountResponse;
import com.sample.agrofoods.Models.ProductDetailsResponse;
import com.sample.agrofoods.Models.ProductVariationResponse;
import com.sample.agrofoods.Models.SpinnerModel;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sample.agrofoods.Utilities.capitalize;


public class ProductDetailsActivity extends AppCompatActivity {
    //    @BindView(R.id.viewPager)
//    ViewPager viewPager;
//    @BindView(R.id.pageIndicatorView)
//    PageIndicatorView pageIndicatorView;
    @BindView(R.id.txtProductName)
    TextView txtProductName;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.txtDisPrice)
    TextView txtDisPrice;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.btnGotoCart)
    Button btnGotoCart;
    @BindView(R.id.btnAddtocart)
    Button btnAddtocart;
    @BindView(R.id.bootamLayout)
    CardView bootamLayout;
    @BindView(R.id.progressBarMain)
    ProgressBar progressBarMain;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;

    AppController appController;
    List<String> viewPagerItemslist = new ArrayList<>();
    @BindView(R.id.product_minus)
    TextView productMinus;
    @BindView(R.id.product_quantity)
    TextView productQuantity;
    @BindView(R.id.product_plus)
    TextView productPlus;
    @BindView(R.id.txtWeight)
    TextView txtWeight;

    @BindView(R.id.linear_add)
    LinearLayout linear_add;

    @BindView(R.id.linear_add1)
    LinearLayout linear_add1;

    @BindView(R.id.linear3)
    LinearLayout linear3;

    @BindView(R.id.llinear_cart)
    LinearLayout llinear_cart;

    @BindView(R.id.GotoCart)
    Button GotoCart;

    @BindView(R.id.image_product)
    ImageView image_product;

    @BindView(R.id.spinner_variation)
    Spinner spinner_variation;

    @BindView(R.id.relative_drop)
    RelativeLayout relative_drop;
    @BindView(R.id.txt_offer)
    TextView txtOffer;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.text_count)
    TextView textCount;
    @BindView(R.id.relative_count)
    RelativeLayout relativeCount;
    String userid;
    int cartindex;
    int count = 1;

    String id, title, stat, tokenValue;
    String productIdsSet;
    String PId, variationid, token;
    ArrayList<SpinnerModel> customerlist;
    private PrefManager prefManager;
    String cartTypeSet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadOncreateData();

    }

    private void loadOncreateData() {

        prefManager = new PrefManager(ProductDetailsActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (getIntent().getExtras() != null) {
            id = getIntent().getStringExtra("productId");
            Log.e("PRODUCTID", "" + id);
            title = getIntent().getStringExtra("productName");
            stat = getIntent().getStringExtra("status");
        }
        txtTitle.setText(capitalize(title));
        //  getSupportActionBar().setTitle();
        appController = (AppController) getApplication();

        if (appController.isConnection()) {
            CartCount(token);
            invalidateOptionsMenu();
            getProductsData(id);
            //  getVariations(id);

        } else {
            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ProductDetailsActivity.this, DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
        // quantity decrease
        productMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (count > 1) {
                    count--;
                    productQuantity.setText("" + count);
                }
            }
        });

        // quantity increase
        productPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                productQuantity.setText("" + count);
            }
        });


        btnGotoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        GotoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    private void getVariations(String id) {
        customerlist = new ArrayList<>();
        ProgressDialog progressDialog = new ProgressDialog(ProductDetailsActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<ProductVariationResponse> call = RetrofitClient.getInstance().getApi().ProductVariations(id);
        call.enqueue(new Callback<ProductVariationResponse>() {
            @Override
            public void onResponse(Call<ProductVariationResponse> call, Response<ProductVariationResponse> response) {
                if (response.isSuccessful()) ;
                ProductVariationResponse productVariationResponse = response.body();
                if (productVariationResponse.getCode() == 200) {
                    progressDialog.dismiss();
                    List<ProductVariationResponse.DataBean> dataBeanList = productVariationResponse.getData();

//                    for (int i=0;i<dataBeanList.size();i++){
//                        customerlist.add(new SpinnerModel(String.valueOf(dataBeanList.get(i).getId()),dataBeanList.get(i).getValue(),dataBeanList.get(i).getPrice(),
//                                dataBeanList.get(i).getDiscount()));
//                    }

                    SpinnerAdapter preriotyAdapter = new SpinnerAdapter(customerlist, ProductDetailsActivity.this);
                    spinner_variation.setAdapter(preriotyAdapter);
                    spinner_variation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            variationid = customerlist.get(position).getId();
                            //  txtProductName.setText(capitalize(dataBean.getName()));
                            txtPrice.setText(getResources().getString(R.string.Rs) + customerlist.get(position).getDiscountprice());
                            txtDisPrice.setText(getResources().getString(R.string.Rs) + customerlist.get(position).getPrice());
                            txtDisPrice.setPaintFlags(txtDisPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            txtDisPrice.setTextColor(Color.RED);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                } else if (productVariationResponse.getCode() == 401) {
                    progressDialog.dismiss();
                    //Toast.makeText(ProductDetailsActivity.this, "No Data!..", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ProductVariationResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProductDetailsActivity.this, "Server Error..", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void getProductsData(String id) {
        customerlist = new ArrayList<>();
        ProgressDialog progressDialog = new ProgressDialog(ProductDetailsActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<ProductDetailsResponse> call = RetrofitClient.getInstance().getApi().ProductDetails(id, token);
        call.enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, Response<ProductDetailsResponse> response) {
                if (response.isSuccessful()) ;
                ProductDetailsResponse productDetailsResponse = response.body();

                if (productDetailsResponse.getCode() == 200) {
                    progressDialog.dismiss();
                    ProductDetailsResponse.DataBean dataBean = productDetailsResponse.getData();
                    txtProductName.setText(capitalize(dataBean.getName()));

                    txtDescription.setText(dataBean.getDescription());

                    Picasso.get().load(dataBean.getImage()).error(R.drawable.placeholder).into(image_product);

                    String v_status = String.valueOf(dataBean.getVeriation());
                    relative_drop.setVisibility(View.VISIBLE);
                    List<ProductDetailsResponse.DataBean.VeriationsBean> veriationsBeanList = dataBean.getVeriations();

                    for (int i = 0; i < veriationsBeanList.size(); i++) {
                        customerlist.add(new SpinnerModel(String.valueOf(veriationsBeanList.get(i).getId()), veriationsBeanList.get(i).getValue(), veriationsBeanList.get(i).getPrice(),
                                veriationsBeanList.get(i).getSale_price(), String.valueOf(veriationsBeanList.get(i).getIn_cart()), String.valueOf(veriationsBeanList.get(i).getOffer())));

                    }

                    SpinnerAdapter preriotyAdapter = new SpinnerAdapter(customerlist, ProductDetailsActivity.this);
                    spinner_variation.setAdapter(preriotyAdapter);
                    spinner_variation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            variationid = customerlist.get(position).getId();
                            //  txtProductName.setText(capitalize(dataBean.getName()));
                            txtPrice.setText(getResources().getString(R.string.Rs) + customerlist.get(position).getDiscountprice());
                            txtDisPrice.setText(getResources().getString(R.string.Rs) + customerlist.get(position).getPrice());
                            txtDisPrice.setPaintFlags(txtDisPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            txtDisPrice.setTextColor(Color.RED);

                            txtOffer.setText(customerlist.get(position).getOffer() + "%");

                            if (customerlist.get(position).getIn_cart().equals("1")) {
                                linear3.setVisibility(View.GONE);
                                llinear_cart.setVisibility(View.GONE);
                                GotoCart.setVisibility(View.VISIBLE);
                            } else {
                                linear3.setVisibility(View.VISIBLE);
                                llinear_cart.setVisibility(View.VISIBLE);
                                GotoCart.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    btnAddtocart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            addtocartData(dataBean.getId(), productQuantity.getText().toString());

                        }
                    });

                } else if (productDetailsResponse.getCode() == 401) {
                    progressDialog.dismiss();
                    Toast.makeText(ProductDetailsActivity.this, productDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (productDetailsResponse.getCode() == 204) {
                    progressDialog.dismiss();
                    Toast.makeText(ProductDetailsActivity.this, productDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProductDetailsActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void addtocartData(int id, String toString) {
        if (appController.isConnection()) {
            ProgressDialog progressDialog = new ProgressDialog(ProductDetailsActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            Call<AddCartResponse> cartResponseCall = RetrofitClient.getInstance().getApi().AddCart(String.valueOf(id), toString, token, variationid);
            cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                @Override
                public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                    if (response.isSuccessful()) ;
                    AddCartResponse addCartResponse = response.body();

                    if (addCartResponse.getCode() == 200) {
                        progressDialog.dismiss();
                        Toast.makeText(ProductDetailsActivity.this, addCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        linear3.setVisibility(View.GONE);
                        llinear_cart.setVisibility(View.GONE);
                        GotoCart.setVisibility(View.VISIBLE);

                          CartCount(token);

                    } else if (addCartResponse.getCode() == 401) {
                        progressDialog.dismiss();
                        Toast.makeText(ProductDetailsActivity.this, addCartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddCartResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(this, "No Internet Connection..", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        loadOncreateData();
        CartCount(token);
        // invalidateOptionsMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();
        CartCount(token);
        // invalidateOptionsMenu();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
//        final MenuItem menuItem = menu.findItem(R.id.action_cart);
//        menuItem.setIcon(Converter.convertLayoutToImage(ProductDetailsActivity.this, cartindex, R.drawable.ic_actionbar_bag));
//        return true;
//    }

    public void CartCount(String token) {

        Call<CartCountResponse> call = RetrofitClient.getInstance().getApi().CartCount(token);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) ;
                CartCountResponse cartCountResponse = response.body();

                if (cartCountResponse.getCode() == 200) {

                    CartCountResponse.DataBean dataBean = cartCountResponse.getData();
                    cartindex = dataBean.getCount();
                    Log.e("CART_INDEX", "" + cartindex);

                    if (String.valueOf(cartindex).equals("0")) {
                        relativeCount.setVisibility(View.GONE);
                    } else {
                        relativeCount.setVisibility(View.VISIBLE);
                    }

                    textCount.setText(String.valueOf(cartindex));

                } else if (cartCountResponse.getCode() == 204) {

                    Toast.makeText(getApplicationContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();


                }

            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "Server error...", Toast.LENGTH_SHORT).show();

            }
        });


    }

//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//
//            case android.R.id.home:
//                if (stat.equals("home")) {
//                    Intent intent2 = new Intent(ProductDetailsActivity.this, DashBoard.class);
//                    startActivity(intent2);
//                } else if (stat.equals("listing")) {
//                    onBackPressed();
//                };
//                break;
//
//            case R.id.action_cart:
//                Intent intent = new Intent(ProductDetailsActivity.this, CartActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                break;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (stat.equals("home")) {
            Intent intent2 = new Intent(ProductDetailsActivity.this, DashBoard.class);
            intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent2);
        } else if (stat.equals("listing")) {
            finish();
        }
        else if (stat.equals("search")){
            finish();
        }
    }

    @OnClick({R.id.img_back, R.id.img_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                if (stat.equals("home")) {
                    Intent intent2 = new Intent(ProductDetailsActivity.this, DashBoard.class);
                    startActivity(intent2);
                } else if (stat.equals("listing")) {
                    onBackPressed();
                }
                else if (stat.equals("search")){
                    onBackPressed();
                }

                break;
            case R.id.img_cart:
                Intent intent = new Intent(ProductDetailsActivity.this, CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }
}
