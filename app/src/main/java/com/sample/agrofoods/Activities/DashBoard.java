package com.sample.agrofoods.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sample.agrofoods.Fragments.CartFragment;
import com.sample.agrofoods.Fragments.FindFoodFragment;
import com.sample.agrofoods.Fragments.MenuFragment;
import com.sample.agrofoods.Fragments.NotificationFragment;
import com.sample.agrofoods.Fragments.ProfileFragment;
import com.sample.agrofoods.Fragments.QuickOrderFragment;
import com.sample.agrofoods.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class DashBoard extends BaseActivity {

    final Fragment fragment1 = new FindFoodFragment();
    final Fragment fragment2 = new QuickOrderFragment();
    final Fragment fragment3 = new CartFragment();
    final Fragment fragment4 = new NotificationFragment();
    final Fragment fragment5 = new MenuFragment();
    final Fragment fragment6 = new ProfileFragment();
    BottomNavigationView bottomNavigationView;
    public FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment active = fragment1;
    Fragment active2 = fragment2;
    Fragment active3 = fragment3;
    Fragment active4 = fragment4;
    Fragment active5 = fragment5;
    Fragment active6 = fragment6;
    private Boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.continer, new FindFoodFragment(), "homee");
        fragmentTransaction.commit();


        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setItemIconTintList(null);

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.findfood:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.continer, new FindFoodFragment(), "homee").hide(active);
                    fragmentTransaction.commit();
                    active = fragment1;
                    return true;


                case R.id.quickorder:
//                    fragmentManager = getSupportFragmentManager();
//                    fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.continer, new QuickOrderFragment(), "").hide(active2);
//                    fragmentTransaction.commit();
//                    active2 = fragment2;

                    startActivity(new Intent(getApplicationContext(),SearchActivity.class));
                    return true;


                    case R.id.notifications:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.continer, new NotificationFragment(), "").hide(active4);
                    fragmentTransaction.commit();
                    active4 = fragment4;
                    return true;
                case R.id.profile:
//                    fragmentManager = getSupportFragmentManager();
//                    fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.continer, new ProfileFragment(), "").hide(active6);
//                    fragmentTransaction.commit();
//                    active6 = fragment6;

                    startActivity(new Intent(getApplicationContext(),MyAccountActivity.class));
                    return true;

                    case R.id.menu:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.continer, new MenuFragment(), "").hide(active5);
                    fragmentTransaction.commit();
                    active5 = fragment5;
                    return true;

            }

            return false;
        }

    };

    @Override
    public void onBackPressed() {

        final AlertDialog dialogBuilder = new AlertDialog.Builder(DashBoard.this).create();
        LayoutInflater inflater1 = getLayoutInflater();
        View dialogView = inflater1.inflate(R.layout.alert_exit, null);
        Button btn_yes = dialogView.findViewById(R.id.btn_yes);
        Button btn_no = dialogView.findViewById(R.id.btn_no);
        ImageView img_cacel = dialogView.findViewById(R.id.img_cacel);

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (exit) {
                    DashBoard.super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });
        img_cacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });
        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

//                if (exit) {
//                    super.onBackPressed();
//                    moveTaskToBack(true);
//                    Process.killProcess(Process.myPid());
//                    System.exit(1);
//                    return;
//                }
        this.exit = true;
        //  Toast.makeText(DashBoardAdmin.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                exit = false;
            }
        }, 5000);
    }
}
