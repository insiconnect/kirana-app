package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Adapters.OrdersListAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.OrderDetailsResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersListActivity extends AppCompatActivity {
    AppController appController;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txtSubtotal)
    TextView txtSubtotal;
//    @BindView(R.id.txtDeliverCharges)
//    TextView txtDeliverCharges;
    @BindView(R.id.txtamntPaid)
    TextView txtamntPaid;
    @BindView(R.id.txtPayMode)
    TextView txtPayMode;
    @BindView(R.id.layoutParent)
    LinearLayout layoutParent;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.order_status)
    TextView orderStatus;
    @BindView(R.id.txtExpectedDate)
    TextView txtExpectedDate;

    @BindView(R.id.txt_address)
    TextView txt_address;
    private PrefManager pref;
    OrdersListAdapter ordersListAdapter;

    String userId, tokenValue, deviceId, orderId;
    String[] descriptionData = {"OrderPlace", "InProgress", "Shipped", "delivered", "Completed"};
    boolean cartStatus;
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Orders Items");
        appController = (AppController) getApplication();


        // Displaying user information from shared preferences
        pref = new PrefManager(getApplicationContext());

        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (getIntent() != null) {
            orderId = getIntent().getStringExtra("Order_ID");
            cartStatus = getIntent().getBooleanExtra("Checkout", false);
        }

        if (appController.isConnection()) {

            prepareOrderListData(orderId);

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                //    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
    }

    private void prepareOrderListData(String orderId) {

        ProgressDialog progressDialog=new ProgressDialog(OrdersListActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<OrderDetailsResponse> call= RetrofitClient.getInstance().getApi().OrderDetails(orderId);
        call.enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                if (response.isSuccessful());
                OrderDetailsResponse orderDetailsResponse=response.body();
                if (orderDetailsResponse.getCode() == 200){
                    progressDialog.dismiss();
                    OrderDetailsResponse.DataBean dataBean=orderDetailsResponse.getData();

                    txtSubtotal.setText(getResources().getString(R.string.Rs) + " " + dataBean.getGrand_total());
                  //  txtDeliverCharges.setText(getResources().getString(R.string.Rs) + " " + orderDetailsBean.getShippingCharges());
                    txtPayMode.setText("Payment Mode : " + dataBean.getPayment_method());
                    orderStatus.setText("Order status : " + dataBean.getStatus());
                    txtExpectedDate.setText("Expected Delivery Date : "+dataBean.getExpert_date());
                    txtamntPaid.setText(getResources().getString(R.string.Rs) + " " + dataBean.getSub_total());

                    OrderDetailsResponse.DataBean.AddressBean addressBean=dataBean.getAddress();
                    txt_address.setText(addressBean.getAddress_line1()+","+addressBean.getAddress_line2()+","+
                            addressBean.getArea()+","+addressBean.getCity()+","+addressBean.getState()+","+addressBean.getPin_code());
                    List<OrderDetailsResponse.DataBean.OrderItemsBean> orderItemsBeanList=dataBean.getOrder_items();

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    ordersListAdapter = new OrdersListAdapter(OrdersListActivity.this, orderItemsBeanList);
                    recyclerView.setAdapter(ordersListAdapter);

                }
                else if (orderDetailsResponse.getCode() == 401){
                    progressDialog.dismiss();
                   // Toast.makeText(OrdersListActivity.this, "No Data Found!...", Toast.LENGTH_SHORT).show();
                }

                else if (orderDetailsResponse.getCode() == 204){
                    progressDialog.dismiss();
                     Toast.makeText(OrdersListActivity.this, "No Data Found!...", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(OrdersListActivity.this, "", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                if (cartStatus) {
                    Intent intent = new Intent(OrdersListActivity.this, DashBoard.class);
                    intent.putExtra("Checkout", cartStatus);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(OrdersListActivity.this,
                            MyOrdersActivity.class);
                    intent.putExtra("Checkout", cartStatus);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (cartStatus) {
            Intent intent = new Intent(OrdersListActivity.this, DashBoard.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Checkout", cartStatus);
            startActivity(intent);
        } else {
            Intent intent = new Intent(OrdersListActivity.this, MyOrdersActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Checkout", cartStatus);
            startActivity(intent);
        }
    }
}
