package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.ResendOtpREsponse;
import com.sample.agrofoods.Models.VerifyOtpResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VerifyOTPActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.otp_view)
    OtpTextView otpView;
    @BindView(R.id.verify_btn)
    Button verifyBtn;
    @BindView(R.id.txtresend)
    TextView txtresend;
    String mobile,otpText,otp;
    AppController appController;
    PrefManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        session = new PrefManager(this);
        appController = (AppController) getApplication();
        if (getIntent() != null) {
            mobile = getIntent().getStringExtra("Mobile_number");
         //   otpView.setOTP(otp);
            char first = mobile.charAt(0);
            String substring = mobile.substring(Math.max(mobile.length() - 2, 0));
            textView.setText("Please type the verification code sent to\n +91"+first+"XXXXXXX"+substring);

        }
        verifyBtn.setOnClickListener(this);
        txtresend.setOnClickListener(this);

        otpView.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            @Override
            public void onOTPComplete(String otp) {
                // fired when user has entered the OTP fully.
                otpText = otp;
                // Toast.makeText(LoginWithMobileOtpActivity.this, "The OTP is " + otp, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onClick(View v) {
        if (v == verifyBtn) {

            if ((otpText == null || otpText.equals("")) || otpText.length() != 6) {
                Toast.makeText(VerifyOTPActivity.this, "Enter OTP to Verify", Toast.LENGTH_SHORT).show();
                return;
            } else {
                boolean isConnected = appController.isConnection();
                if (isConnected) {

                    ProgressDialog progressDialog=new ProgressDialog(VerifyOTPActivity.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                    Call<VerifyOtpResponse> call= RetrofitClient.getInstance().getApi().VerifyOtp(mobile,otpText);
                    call.enqueue(new Callback<VerifyOtpResponse>() {
                        @Override
                        public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                            if (response.isSuccessful());
                            VerifyOtpResponse verifyOtpResponse=response.body();

                            if (verifyOtpResponse.getCode() == 200){
                                progressDialog.dismiss();
                                Toast.makeText(VerifyOTPActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                               // List<VerifyOtpResponse.DataBean> dataBean=verifyOtpResponse.getData();

                                for (VerifyOtpResponse.DataBean dataBean:verifyOtpResponse.getData()){
                                    session.Login(dataBean.getName(),dataBean.getPhone(),dataBean.getToken());
                                }

                                Intent intent = new Intent(VerifyOTPActivity.this, DashBoard.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                            else if (verifyOtpResponse.getCode() == 401){
                                progressDialog.dismiss();
                                Toast.makeText(VerifyOTPActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(VerifyOTPActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else {
                    Toast.makeText(VerifyOTPActivity.this, "No Internet Connection!...", Toast.LENGTH_SHORT).show();

                }

            }

        }

        if (v == txtresend){

            boolean isConnected = appController.isConnection();
            if (isConnected) {

                ProgressDialog progressDialog=new ProgressDialog(VerifyOTPActivity.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                Call<ResendOtpREsponse> call=RetrofitClient.getInstance().getApi().ResendOtp(mobile);
                call.enqueue(new Callback<ResendOtpREsponse>() {
                    @Override
                    public void onResponse(Call<ResendOtpREsponse> call, Response<ResendOtpREsponse> response) {
                        if (response.isSuccessful());
                        ResendOtpREsponse resendOtpREsponse=response.body();

                        try {
                            if (resendOtpREsponse.getCode() == 200){
                                progressDialog.dismiss();
                                Toast.makeText(VerifyOTPActivity.this, resendOtpREsponse.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                            else if (resendOtpREsponse.getCode() == 401){
                                progressDialog.dismiss();
                                Toast.makeText(VerifyOTPActivity.this, resendOtpREsponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResendOtpREsponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(VerifyOTPActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();

                    }
                });

            }
            else {
                Toast.makeText(VerifyOTPActivity.this, "No Internet Connection!...", Toast.LENGTH_SHORT).show();
            }

        }

    }
}
