package com.sample.agrofoods.Models;

public class RegistrationResponse {


    /**
     * code : 409
     * message : The email has already been taken.
     */

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
