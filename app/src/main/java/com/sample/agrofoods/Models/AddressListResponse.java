package com.sample.agrofoods.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressListResponse {


    /**
     * code : 200
     * message : User Address
     * data : [{"id":5,"first_name":"ganesh","last_name":"chintu","address_line1":null,"address_line2":null,"email":"ganesh44@gmail.com","phone":"9347432387","alternate_phone":null,"state":null,"city":null,"area":null,"pin_code":"500606","address":"kukatpally","type":null,"default":0},{"id":6,"first_name":"ganesh","last_name":"chintu","address_line1":"2-78","address_line2":null,"email":"ganesh44@gmail.com","phone":"9347432387","alternate_phone":"9876543210","state":"telangana","city":"hyderabad","area":"kukatpally","pin_code":"500606","address":"kukatpally","type":null,"default":1}]
     */

    private int code;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 5
         * first_name : ganesh
         * last_name : chintu
         * address_line1 : null
         * address_line2 : null
         * email : ganesh44@gmail.com
         * phone : 9347432387
         * alternate_phone : null
         * state : null
         * city : null
         * area : null
         * pin_code : 500606
         * address : kukatpally
         * type : null
         * default : 0
         */

        private int id;
        private String first_name;
        private String last_name;
        private String address_line1;
        private String address_line2;
        private String email;
        private String phone;
        private String alternate_phone;
        private String state;
        private String city;
        private String area;
        private String pin_code;
        private String address;
        private String type;
        private int defaults;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getAddress_line1() {
            return address_line1;
        }

        public void setAddress_line1(String address_line1) {
            this.address_line1 = address_line1;
        }

        public String getAddress_line2() {
            return address_line2;
        }

        public void setAddress_line2(String address_line2) {
            this.address_line2 = address_line2;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAlternate_phone() {
            return alternate_phone;
        }

        public void setAlternate_phone(String alternate_phone) {
            this.alternate_phone = alternate_phone;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getPin_code() {
            return pin_code;
        }

        public void setPin_code(String pin_code) {
            this.pin_code = pin_code;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getDefaults() {
            return defaults;
        }

        public void setDefaults(int defaults) {
            this.defaults = defaults;
        }
    }
}
