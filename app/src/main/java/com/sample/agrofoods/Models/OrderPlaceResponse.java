package com.sample.agrofoods.Models;

import java.util.List;

public class OrderPlaceResponse {


    /**
     * code : 200
     * order_id : ORD000015
     * message : Order Placed Successfully
     * data : []
     */

    private int code;
    private String order_id;
    private String message;
    private List<?> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}
