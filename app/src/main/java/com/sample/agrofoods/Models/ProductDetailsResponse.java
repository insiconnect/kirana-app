package com.sample.agrofoods.Models;

import java.util.List;

public class ProductDetailsResponse {


    /**
     * code : 200
     * message : Procut details
     * data : {"id":292,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/lAeTlu3qv61ab0GQgkfrkxmobu7YzFlcMkdVcFms.png","name":"Bambino Vermicelli","veriation":2,"description":"Bambino Vermicelli","veriations":[{"id":36,"qty":"1","in_cart":1,"stock":12,"value":"400 gm","price":"40.00","offer":7.5,"sale_price":"37.00"},{"id":70,"qty":0,"in_cart":0,"stock":12,"value":"800 gm","price":"71.00","offer":4.23,"sale_price":"68.00"}],"product_images":[]}
     */

    private int code;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 292
         * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/lAeTlu3qv61ab0GQgkfrkxmobu7YzFlcMkdVcFms.png
         * name : Bambino Vermicelli
         * veriation : 2
         * description : Bambino Vermicelli
         * veriations : [{"id":36,"qty":"1","in_cart":1,"stock":12,"value":"400 gm","price":"40.00","offer":7.5,"sale_price":"37.00"},{"id":70,"qty":0,"in_cart":0,"stock":12,"value":"800 gm","price":"71.00","offer":4.23,"sale_price":"68.00"}]
         * product_images : []
         */

        private int id;
        private String image;
        private String name;
        private int veriation;
        private String description;
        private List<VeriationsBean> veriations;
        private List<?> product_images;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getVeriation() {
            return veriation;
        }

        public void setVeriation(int veriation) {
            this.veriation = veriation;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<VeriationsBean> getVeriations() {
            return veriations;
        }

        public void setVeriations(List<VeriationsBean> veriations) {
            this.veriations = veriations;
        }

        public List<?> getProduct_images() {
            return product_images;
        }

        public void setProduct_images(List<?> product_images) {
            this.product_images = product_images;
        }

        public static class VeriationsBean {
            /**
             * id : 36
             * qty : 1
             * in_cart : 1
             * stock : 12
             * value : 400 gm
             * price : 40.00
             * offer : 7.5
             * sale_price : 37.00
             */

            private int id;
            private String qty;
            private int in_cart;
            private int stock;
            private String value;
            private String price;
            private double offer;
            private String sale_price;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public int getIn_cart() {
                return in_cart;
            }

            public void setIn_cart(int in_cart) {
                this.in_cart = in_cart;
            }

            public int getStock() {
                return stock;
            }

            public void setStock(int stock) {
                this.stock = stock;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public double getOffer() {
                return offer;
            }

            public void setOffer(double offer) {
                this.offer = offer;
            }

            public String getSale_price() {
                return sale_price;
            }

            public void setSale_price(String sale_price) {
                this.sale_price = sale_price;
            }
        }
    }
}
