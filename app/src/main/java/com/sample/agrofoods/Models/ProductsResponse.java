package com.sample.agrofoods.Models;

public class ProductsResponse {
    String p_name,p_price;
    int p_image;

    public ProductsResponse(String p_name, String p_price, int p_image) {
        this.p_name = p_name;
        this.p_price = p_price;
        this.p_image = p_image;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_price() {
        return p_price;
    }

    public void setP_price(String p_price) {
        this.p_price = p_price;
    }

    public int getP_image() {
        return p_image;
    }

    public void setP_image(int p_image) {
        this.p_image = p_image;
    }
}
