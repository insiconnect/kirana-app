package com.sample.agrofoods.Models;

import java.util.List;

public class MyOrdersResponse {


    /**
     * code : 200
     * message : My orders
     * data : [{"order_id":"ORD000003","status":"pending","payment_method":"Cash On Delivery","payment_status":"completed","order_date":"2020-07-11","grand_total":"511.00"},{"order_id":"ORD000004","status":"pending","payment_method":"Cash On Delivery","payment_status":"completed","order_date":"2020-07-11","grand_total":"900.00"},{"order_id":"ORD000005","status":"pending","payment_method":"Cash On Delivery","payment_status":"completed","order_date":"2020-07-11","grand_total":"900.00"},{"order_id":"ORD000006","status":"decline","payment_method":"Cash On Delivery","payment_status":"completed","order_date":"2020-07-13","grand_total":"540.00"}]
     */

    private int code;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * order_id : ORD000003
         * status : pending
         * payment_method : Cash On Delivery
         * payment_status : completed
         * order_date : 2020-07-11
         * grand_total : 511.00
         */

        private String order_id;
        private String status;
        private String payment_method;
        private String payment_status;
        private String order_date;
        private String grand_total;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getGrand_total() {
            return grand_total;
        }

        public void setGrand_total(String grand_total) {
            this.grand_total = grand_total;
        }
    }
}
