package com.sample.agrofoods.Models;

public class ProfileResponse {

    /**
     * code : 200
     * message : User Profile
     * data : {"id":16,"name":"raju","phone":"9347432387","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/raju/XOZf6FsN3pV9s5v68718MdDORkmtjOohiptxrH70.png","email":"raju456@gmail.com","token":"1QnTjeXmXS2WhKz8Kt1Q6EjK5wy2MpNa"}
     */

    private int code;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 16
         * name : raju
         * phone : 9347432387
         * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/raju/XOZf6FsN3pV9s5v68718MdDORkmtjOohiptxrH70.png
         * email : raju456@gmail.com
         * token : 1QnTjeXmXS2WhKz8Kt1Q6EjK5wy2MpNa
         */

        private int id;
        private String name;
        private String phone;
        private String image;
        private String email;
        private String token;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}